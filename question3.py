import sqlite3 as sq
conn=sq.connect('database.sqlite')
c=conn.cursor()
#1
print('\n')
print('HomeTeam, FTHG (number of home goals scored in a game) and FTAG (number of away goals scored in a game)')
print('\n')
c.execute("SELECT HomeTeam,FTHG,FTAG FROM Matches WHERE Season=2010 AND HomeTeam='Aachen' ORDER BY FTHG DESC")
j=c.fetchall()
for k in j:
    print(k)




#2
print('\n')
print('The total number of home games each team won during the 2016 season in descending order of number of home games from the Matches table.')
print('\n')
c.execute("SELECT COUNT(*) FROM Matches WHERE FTHG>FTAG AND Season=2016")
j=c.fetchall()
print('The Number IS',j[0][0])

#3

print('\n')
print('The first ten rows from the Unique_Teams table')
print('\n')
c.execute("SELECT * FROM Unique_Teams LIMIT 10 ")
j=c.fetchall()
for k in j:
    print(k)

#4
print('\n')
print('Match_ID and Unique_Team_ID with the corresponding Team_Name from the Unique_Teams and Teams_in_Matches tables')
print('Using Where Statement')
print('\n')
c.execute("SELECT * FROM Unique_Teams,Teams_in_Matches WHERE Unique_Teams.Unique_Team_ID=Teams_in_Matches.Unique_Team_ID")

k=c.fetchall()
for i in k:
    pass
    #print(i)
print('\n')
print('Using Join Statemnt')
c.execute("SELECT * FROM Unique_Teams INNER JOIN Teams_in_Matches ON Unique_Teams.Unique_Team_ID=Teams_in_Matches.Unique_Team_ID")
l=c.fetchall()
print('Total',len(l),'rows')



#5
print('\n')
print('joins together the Unique_Teams data table and the Teams table, and returns the first 10 rows.')
c.execute("SELECT * FROM Unique_Teams AS U INNER JOIN Teams AS T ON T.TeamName=U.TeamName LIMIT 10")
l=c.fetchall()
for i in l:
    print(i)

#6

print('\n')
print('Unique_Team_ID and TeamName from the Unique_Teams table and AvgAgeHome, Season and ForeignPlayersHome from the Teams table. Only return the first five rows')
c.execute("SELECT Unique_Team_ID,T.TeamName ,AvgAgeHome,Season,ForeignPlayersHome FROM  Unique_Teams  AS U INNER JOIN Teams AS T ON U.TeamName=T.TeamName LIMIT 5")
      
l=c.fetchall()
for i in l:
    print(i)

#7

print('\n')
print('highest Match_ID for each team that ends in a “y” or a “r”. Along with the maximum Match_ID, display the Unique_Team_ID from the Teams_in_Matches table and the TeamName from the Unique_Teams table')
c.execute("SELECT MAX(Match_ID),T.Unique_Team_ID,U.TeamName FROM Teams_in_Matches AS T INNER JOIN Unique_Teams  AS U ON T.Unique_Team_ID=U.Unique_Team_ID WHERE (TeamName LIKE '%y') OR (TeamName LIKE '%r') GROUP BY T.Unique_Team_ID ,U.TeamName")
l=c.fetchall()
for i in l:
    print(i)
