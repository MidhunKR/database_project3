import sqlite3 as sq
conn=sq.connect('database.sqlite')
c=conn.cursor()

print('\n')
print('Number of the rows in the Teams table')
c.execute("SELECT COUNT(*) FROM Teams")
o=c.fetchall()
print(o[0][0])

print('\n')
print(' Seasons')
c.execute("SELECT DISTINCT Season FROM Teams")
l=c.fetchall()
for i in l:
    print(i)

print('\n')
print('Max And Minimum Seating Capacity')

c.execute("SELECT MIN(StadiumCapacity),MAX(StadiumCapacity) FROM Teams")
l=c.fetchall()
print('Minimum Value',l[0][0])
print('Maximum Value',l[0][1])

print('\n')
print('The Sum Of All squad players')
c.execute("SELECT SUM(KaderHome) FROM Teams WHERE Season=2014")
l=c.fetchall()
print('The sum is',l[0][0])

print('\n')
c.execute("SELECT AVG(FTAG) FROM Matches WHERE HomeTeam='Man United'")
l=c.fetchall()
print('The average is',l[0][0])
