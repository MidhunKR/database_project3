import sqlite3 as sq
conn=sq.connect('database.sqlite')
c=conn.cursor()
c.execute("SELECT  HomeTeam,AwayTeam,Season FROM Matches WHERE Season=2015 AND FTHG=5")
i=c.fetchall()
#2
print('\n')
print('All Teams Who Played In Season 2015 and Full time Home Goals (FTHG) = 5')
print('\n')
for j in i:
    print(j)
#3    
print('\n')
print('Details of the matches where Arsenal is the Home Team and  Full Time Result (FTR) is “A” (Away')
print('\n')
c.execute("SELECT * FROM Matches WHERE HomeTeam='Arsenal' AND FTR='A'")
a=c.fetchall()
for i in a:
    print(i)

#4
print('\n')
print('Matches from the 2012 season till the 2015 season where Away Team is Bayern Munich and Full time Away Goals (FTHG) > 2')
c.execute("SELECT * FROM Matches WHERE Season IN(2012,2013,2014,2015) AND AwayTeam='Bayern Munich' AND FTHG>2")
b=c.fetchall()
for i in b:
    print(i)

#5
print('\n')
print('The matches where the Home Team name begins with “A” and Away Team name begins with  M')
print('\n')
c.execute("SELECT * FROM Matches WHERE HomeTeam LIKE 'A%' AND AwayTeam LIKE 'M%' ")
d=c.fetchall()
for i in d:
    print(i)

